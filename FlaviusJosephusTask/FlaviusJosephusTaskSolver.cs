﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FlaviusJosephusTask
{
    public static class FlaviusJosephusTaskSolver
    {
        /// <summary>
        /// Solve Flavious Josephus Task
        /// </summary>
        /// <param name="n"> Number of people.</param>
        /// <param name="k"> Every K-th will be removed.</param>
        /// <returns> The start position so as not to be removed</returns>
        public static int Solve(int n, int k)
        {
            if(n<1)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }

            if(k<2)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            var listOfPeople = new List<int>(Enumerable.Range(1, n));

            int currentPosition = 0;
            int gap = k - 1;
            while (listOfPeople.Count > 1)
            {
                currentPosition = (currentPosition + gap) % listOfPeople.Count;
                listOfPeople.RemoveAt(currentPosition);
            }

            return listOfPeople.First();
        }

        /// <summary>
        /// Solve Flavious Josephus Task
        /// </summary>
        /// <param name="n"> Number of people.</param>
        /// <param name="k"> Every K-th will be removed.</param>
        /// <returns> The start position so as not to be removed</returns>
        public static int SolveRecurrent(int n, int k)
        {
            if (n < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }

            if (k < 2)
            {
                throw new ArgumentOutOfRangeException(nameof(k));
            }

            int recurrentSolution = 1;
            int gap = k - 1;
            for (int i = 2; i <= n; i++)
            {
                recurrentSolution = 1 + (recurrentSolution + gap) % i;
            }

            return recurrentSolution;
        }
    }
}
