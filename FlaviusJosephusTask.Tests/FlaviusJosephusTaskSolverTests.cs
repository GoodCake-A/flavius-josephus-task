using NUnit.Framework;

namespace FlaviusJosephusTask.Tests
{
    public class FlaviusJosephusTaskSolverTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(41, 2, ExpectedResult = 19)]
        [TestCase(10, 2, ExpectedResult = 5)]
        [TestCase(7, 3, ExpectedResult = 4)]
        [TestCase(100000,3,ExpectedResult = 92620)]
        [TestCase(1000000, 3, ExpectedResult = 637798)]
        public int SolveRecurrentTest(int n, int k)
        {
            return FlaviusJosephusTaskSolver.SolveRecurrent(n, k);
        }

        [TestCase(41, 2, ExpectedResult = 19)]
        [TestCase(10,2,ExpectedResult =5)]
        [TestCase(7, 3, ExpectedResult = 4)]
        [TestCase(100000, 3, ExpectedResult = 92620)]
        [TestCase(1000000, 3, ExpectedResult = 637798)]
        public int SolveTest(int n, int k)
        {
            return FlaviusJosephusTaskSolver.Solve(n, k);
        }

    }
}